<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-27
 * Time: 17:53
 */


require "../../Utils/RouteDispatcher.php";

define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);

//disabling errors and notice in payloads
error_reporting(0);

new RouteDispatcher();
