<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 10:19
 */

namespace APIs\JsonAPI\controllers\task;


use Repositories\Tasks\TasksRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\TaskModel;



class getTask extends baseController
{

    public function execute(array $GET=[])
    {
        try{
            $Methods = new TasksRepository();

            if($GET["id"]!==null){
                $task = $Methods->getTask($GET["id"]);
                echo json_encode(['task'=>$task]);

            }else{
                if($GET["user_id"]!==null){
                    $task = $Methods->getAllTasks($GET["user_id"]);
                    echo json_encode(['tasks'=>$task]);
                }else{
                    parent::returnError("Bad Argument Error");
                }

            }
        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }
}
