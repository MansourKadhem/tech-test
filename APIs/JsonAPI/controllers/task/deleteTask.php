<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 10:18
 */

namespace APIs\JsonAPI\controllers\task;

use Repositories\Tasks\TasksRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\TaskModel;




class deleteTask extends baseController
{
    public function execute(array $GET=[])
    {
        try{
            $Input = parent::parseJson();

            $Methods = new TasksRepository();

            if($Input["id"]!=null){
                $result = $Methods->deleteTask($Input["id"]);
                echo json_encode(['task' =>$result]);
            }else{
                parent::returnError("Bad Argument Error");
            }

        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }

}
