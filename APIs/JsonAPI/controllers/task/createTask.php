<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 10:10
 */
namespace APIs\JsonAPI\controllers\task;


use Repositories\Tasks\TasksRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\TaskModel;



class createTask extends baseController
{

    public function execute(array $GET=[])
    {
        try{

            $Input = parent::parseJson();

            $Methods = new TasksRepository();

            $newTask = new TaskModel($Input["title"],$Input["description"],$Input["status"],$Input['user_id']);

            $result = $Methods->createTask($newTask);

            echo json_encode(['user' =>$result]);

        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }
}








