<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 09:54
 */
namespace APIs\JsonAPI\controllers\user;

use Repositories\Users\UsersRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\UserModel;


class updateUser extends baseController
{
    public function execute(array $GET=[])
    {
        try{
            $Input = parent::parseJson();

            $Methods = new UsersRepository();

            $newUser = new UserModel($Input["id"],$Input["name"],$Input["email"]);

            $result = $Methods->updateUser($newUser);

            echo json_encode(['userUpdated' =>$result]);

        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }
}
