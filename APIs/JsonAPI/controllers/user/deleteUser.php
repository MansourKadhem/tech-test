<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 09:57
 */
namespace APIs\JsonAPI\controllers\user;


use Repositories\Users\UsersRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\UserModel;

class deleteUser extends baseController
{
    public function execute(array $GET = [])
    {
        try{
            $Input = parent::parseJson();

            $Methods = new UsersRepository();

            if($Input["id"]!=null){
                /**
                 * NOTE : Tasks related to a specific user will not be deleted and can be reassigned to an other user
                 */
                $result = $Methods->deleteUser($Input["id"]);
                echo json_encode(['user' =>$result]);
            }else{
                parent::returnError("Bad Argument Error");
            }

        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }
}
