<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 09:48
 */
namespace APIs\JsonAPI\controllers\user;

use Repositories\Users\UsersRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\UserModel;




class getUser extends baseController
{

    public function execute(array $GET=[])
    {
        try{
            $Methods = new UsersRepository();

            if($GET["id"]!==null){

                switch ($GET["id"]){
                    case "all" :{
                        $users = $Methods->getAllUsers();
                        echo json_encode(['user'=>$users]);
                        break;
                    }
                    default :{
                        $user = $Methods->getUser($GET["id"]);
                        echo json_encode(['user'=>$user]);
                        break;
                    }
                }

            }else{
                parent::returnError("Bad Argument Error");
            }
        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }
}
