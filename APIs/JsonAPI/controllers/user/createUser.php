<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 09:05
 */
namespace APIs\JsonAPI\controllers\user;

use Repositories\Users\UsersRepository;
use APIs\JsonAPI\controllers\baseController;
use Models\UserModel;



class createUser extends baseController
{

    public function execute(array $GET=[])
    {
        try{

            $Input = parent::parseJson();

            $Methods = new UsersRepository();

            $newUser = new UserModel("",$Input["name"],$Input["email"]);

            $res =$Methods->createUser($newUser);

            echo json_encode(['user' =>$res]);

        }catch (\Exception $exception){
            parent::returnError($exception->getMessage());
        }
    }

}
