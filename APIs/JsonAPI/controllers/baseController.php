<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 09:12
 */
namespace APIs\JsonAPI\controllers;

/**
 * Class baseController
 * @package APIs\JsonAPI\controllers
 * A base model for the controllers
 */
abstract class baseController
{


    public abstract function execute(array $GET=[]);


    protected function parseJson()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $array = json_decode(json_encode($data), true);
        return $array;
    }

    // Will Return a real http response error.
    protected function returnError($message, $code=400)
    {

        header_remove();
        // will set the response code
        http_response_code($code);

        header('Content-Type: application/json');
        $status = array(
            200 => '200 OK',
            400 => '400 Bad Request',
            404 => '404 Not Found',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
        );
        // will set the status header
        header('Status: '.$status[$code]);

        echo json_encode(['error' => true, 'message' => $message]);

    }


}
