<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-28
 * Time: 08:57
 */
namespace APIs\JsonAPI\routes;




use APIs\JsonAPI\controllers\task\createTask;
use APIs\JsonAPI\controllers\task\deleteTask;
use APIs\JsonAPI\controllers\task\getTask;
use APIs\JsonAPI\controllers\task\updateTask;
use APIs\JsonAPI\controllers\user\createUser;
use APIs\JsonAPI\controllers\user\deleteUser;
use APIs\JsonAPI\controllers\user\getUser;
use APIs\JsonAPI\controllers\user\updateUser;

/**
 * Class RouteMatching
 * @package APIs\JsonAPI\routes
 * This class acts as a repository for the routes that are handled by the API, each route has a name, a controller linked to It and a method.
 */
class RouteMatching
{

    public  $routes= [

    ];


    public function __construct()
    {
        $this->routes=[
             [
                'name'=>User::$DELETE_SINGLE_USER_ROUTE,
                'controller' => deleteUser::class,
                'method' => 'DELETE'
            ],
             [
                'name'=>User::$ADD_SINGLE_USER_ROUTE,
                'controller' => createUser::class,
                'method' => 'POST'
            ],
             [
                'name'=>User::$GET_ALL_USERS_ROUTE,
                'controller' => getUser::class,
                'method' => 'GET'
            ],
             [
                'name'=>User::$GET_SINGLE_USER_ROUTE,
                'controller' => getUser::class,
                'method' => 'GET'
            ],
             [
                'name'=>User::$UPDATE_SINGLE_USER_ROUTE,
                'controller' => updateUser::class,
                'method' => 'PUT'
            ],



             [
                'name'=>Task::$UPDATE_SINGLE_TASK_ROUTE,
                'controller' => updateTask::class,
                'method' => 'PUT'
            ],
             [
                'name'=>Task::$ADD_SINGLE_TASK_ROUTE,
                'controller' => createTask::class,
                'method' => 'POST'
            ],
             [
                'name'=>Task::$DELETE_SINGLE_TASK_ROUTE,
                'controller' => deleteTask::class,
                'method' => 'DELETE'
            ],
             [
                'name'=>Task::$GET_ALL_USER_TASKS_ROUTE,
                'controller' => getTask::class,
                'method' => 'GET'
            ],
             [
                'name'=>Task::$GET_SINGLE_TASK_ROUTE,
                'controller' => getTask::class,
                'method' => 'GET'
            ],

        ];
    }

}
