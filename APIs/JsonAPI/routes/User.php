<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-27
 * Time: 17:54
 */
namespace APIs\JsonAPI\routes;

/**
 * Class User
 * @package APIs\JsonAPI\routes
 * Definition of the Regular Expressions for the routes related to the user
 */
class User
{
    public static $GET_SINGLE_USER_ROUTE="/user\/[0-9]+$/";
    public static $GET_ALL_USERS_ROUTE="/user\/all$/";
    public static $ADD_SINGLE_USER_ROUTE="/user$/";
    public static $UPDATE_SINGLE_USER_ROUTE="/user$/";
    public static $DELETE_SINGLE_USER_ROUTE="/user$/";
}
