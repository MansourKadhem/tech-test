<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-27
 * Time: 18:01
 */
namespace APIs\JsonAPI\routes;

/**
 * Class task
 * @package APIs\JsonAPI\routes
 * Definition of the Regular Expressions for the routes related to the tasks
 */
class task
{
    public static $GET_SINGLE_TASK_ROUTE="/task\/[0-9]+$/";
    public static $GET_ALL_USER_TASKS_ROUTE="/user\/[0-9]+\/tasks$/";
    public static $ADD_SINGLE_TASK_ROUTE="/task$/";
    public static $UPDATE_SINGLE_TASK_ROUTE="/task$/";
    public static $DELETE_SINGLE_TASK_ROUTE="/task$/";
}
