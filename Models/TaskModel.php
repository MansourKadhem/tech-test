<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-23
 * Time: 10:14
 */

namespace Models;


class TaskModel
{
    private $id;
    private $user_id;
    private $title;
    private $description;
    private $creation_date;
    private $status;

    /**
     * TaskModel constructor.
     * @param $user_id
     * @param $title
     * @param $description
     * @param $creation_date
     * @param $status
     */
    public function __construct( $title, $description,$status, $user_id=null, $id=null, $creation_date=null )
    {
        $this->user_id = $user_id;
        $this->title = $title;
        $this->description = $description;
        $this->creation_date = $creation_date;
        $this->status = $status;
        $this->id=$id;
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param null $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }

    /**
     * @param mixed $creation_date
     */
    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }




}
