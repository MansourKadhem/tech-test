<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-23
 * Time: 10:17
 */
namespace Repositories\Tasks;


use Constants\DbConfig;
use database\dbconnect;
use Models\TaskModel;

require 'TaskValidator.php';
require __DIR__ .'/../../database/dbconnect.php';
require __DIR__ .'/../../Constants/DbConfig.php';

class TasksRepository
{


    /**
     * TasksRepository constructor.
     */
    public function __construct()
    {
        try{

            $this->userValidator = new TaskValidator();

            $this->DataBase = new dbconnect(DbConfig::$BD_HOST.":".DbConfig::$DB_PORT,
                DbConfig::$DB_USERNAME,DbConfig::$DB_PASSWORD,DbConfig::$DB_DBNAME);
        }catch(\Exception $exception){

            var_dump($exception);
        }
    }

    public function createTask(TaskModel $taskModel){
        try{
            $this->userValidator->validate($taskModel);
            $user_id=$taskModel->getUserId()==null?"NULL":$taskModel->getUserId();
            $status=$taskModel->getStatus()==null?"NULL":$taskModel->getStatus();
            $title= $taskModel->getTitle();
            $description = $taskModel->getDescription();

            $sqlQuery="Insert into tasks(user_id,description,title,status) values ($user_id,'$description','$title',$status)";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }

    public function updateTask(TaskModel $taskModel){
        try{
            $user_id=$taskModel->getUserId()==null?"NULL":$taskModel->getUserId();
            $title= $taskModel->getTitle();
            $description = $taskModel->getDescription();
            $status=$taskModel->getStatus()==null?"NULL":$taskModel->getStatus();
            $id=$taskModel->getID();
            $sqlQuery="
                UPDATE tasks
                SET 
                    user_id = $user_id,
                    title = '$title',
                    description = '$description',
                    status = $status
                   
                WHERE
                    id=$id;
            ";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }

    public function deleteTask($id){
        try{
            $sqlQuery="
            DELETE FROM tasks
            WHERE
                id=$id;
            ";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }

    public function getTask($task_id){
        try{

            $sqlQuery="
            SELECT id,user_id,title,description,creation_date,status
            FROM tasks
            WHERE id=$task_id
            ORDER BY id ASC
            ";
            return $this->handleQuery($sqlQuery,true);

        }catch(\Exception $exception){
            throw $exception;
        }
    }

    public function getAllTasks($user_id){
        $sqlQuery="
            SELECT id,user_id,title,description,creation_date,status
            FROM tasks
            WHERE user_id=$user_id
            ORDER BY id ASC
            ";
        return $this->handleQuery($sqlQuery,true);
    }

    private function handleQuery($query,$getResult=false){
        $MysqlDb = $this->DataBase->connect();

        $result =$MysqlDb->query($query);
        if($result==true){
            if($getResult==true){
                $QueryResult =[];
                while($row = $result->fetch_assoc()) {
                    array_push($QueryResult,$row);
                }

                return $QueryResult;
            }else{
                return $result;
            }
        }else{
            throw new \Exception($MysqlDb->error,$MysqlDb->errno);
        }

    }

}
