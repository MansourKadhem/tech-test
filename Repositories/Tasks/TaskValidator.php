<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-23
 * Time: 10:17
 */

namespace Repositories\Tasks;

use Models\TaskModel;

class TaskValidator
{

    public function validate(TaskModel $taskModel){
        if($taskModel->getTitle() && $taskModel->getStatus()){
            return true;
        }else{
            throw new \Exception("NotValidated",401);
        }
    }

}
