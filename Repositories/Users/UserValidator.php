<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-22
 * Time: 10:42
 */

namespace Repositories\Users;


use Models\UserModel;


class UserValidator
{


    /**
     * UserValidator constructor.
     */
    public function __construct()
    {
    }

    public function validate(UserModel $userModel){
        if($userModel->getEmail() && $userModel->getName()){
            return true;
        }else{
            throw new \Exception("NotValidated",401);
        }
    }

}
