<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-22
 * Time: 10:42
 */

namespace Repositories\Users;

use Models\UserModel;
use database\dbconnect;
use Constants\DbConfig;

require 'UserValidator.php';
require __DIR__ .'/../../database/dbconnect.php';
require __DIR__ .'/../../Constants/DbConfig.php';



class UsersRepository
{

    private $DataBase;

    private $userValidator;

    /**
     * UsersRepository constructor.
     */
    public function __construct()
    {

        try{

            $this->userValidator = new UserValidator();

            $this->DataBase = new dbconnect(DbConfig::$BD_HOST.":".DbConfig::$DB_PORT,
                DbConfig::$DB_USERNAME,DbConfig::$DB_PASSWORD,DbConfig::$DB_DBNAME);
        }catch(\Exception $exception){

            var_dump($exception);
        }
    }

    public function createUser(UserModel $userModel){
        try{
            $this->userValidator->validate($userModel);
            $name=$userModel->getName();
            $email= $userModel->getEmail();

            $sqlQuery="Insert into users(email,name) values ('$email','$name')";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }


    public function updateUser(UserModel $userModel){
        try{
            $name=$userModel->getName();
            $email= $userModel->getEmail();
            $id=$userModel->getId();
            $sqlQuery="
                UPDATE users
                SET 
                    name = '$name',
                    email = '$email'
                   
                WHERE
                    id=$id;
            ";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }


    public function deleteUser($id){

        try{
            $sqlQuery="
            DELETE FROM users
            WHERE
                id=$id;
            ";
            return $this->handleQuery($sqlQuery);

        }catch(\Exception $exception){
            throw $exception;
        }
    }

    public function getUser($id){
        try{

            $sqlQuery="
            SELECT id,name,email
            FROM users
            WHERE id=$id
            ORDER BY id ASC
            ";
            return $this->handleQuery($sqlQuery,true);

        }catch(\Exception $exception){
            throw $exception;
        }
    }


    public function getAllUsers(){
        $sqlQuery="
            SELECT id,name,email
            FROM users
            ORDER BY id 
            ";
        return $this->handleQuery($sqlQuery,true);
    }


    private function handleQuery($query,$getResult=false){
        $MysqlDb = $this->DataBase->connect();

        $result =$MysqlDb->query($query);


        if($result==true){
            if($getResult==true){
                $QueryResult =[];
                while($row = $result->fetch_assoc()) {
                    array_push($QueryResult,$row);
                }

                return $QueryResult;
            }else{
                return $result;
            }
        }else{
           throw new \Exception($MysqlDb->error,$MysqlDb->errno);
        }

    }

}

