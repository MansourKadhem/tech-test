<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-27
 * Time: 17:51
 */

use APIs\JsonAPI\routes\RouteMatching;

/**
 * Class RouteDispatcher
 * The main routing gateway for the whole application :
 * Will go through the stored routes and compare the requested route against them, when a match is found,
 * a respective controller is called and passed the arguments from the route.
 * The controller will respond with the appropriate payload.
 */
class RouteDispatcher
{


    public function __construct()
    {
       try{
           // Dispatching autoload functionality
           spl_autoload_register(function ($class) {

               $file = dirname(__DIR__) . "/" . str_replace('\\', DIRECTORY_SEPARATOR, $class) . ".php";

               if (file_exists($file)) {

                   require $file;
                   return true;
               }
               return false;
           });


           // Starting the route dispatching

           $RouteMatch = new RouteMatching();

           $routes = $RouteMatch->routes;


           foreach ($routes as $key=>$route) {
               if($this->handleRouteExpression($route['name'])==true){
                   if($_SERVER['REQUEST_METHOD']==$route["method"]){
                       $class = $route['controller'];
                           $newClass = new $class();
                           $newClass->execute($this->handleSubsequentGetParameters());
                           break;
                   }
               }
           }
       }catch(\Exception $exception){
           echo $exception;
       }

    }


    public function handleRouteExpression($route){
       /* var_dump($_GET['url'],$route);*/
        if($_GET['url']){
            $matches=[];
            $res = preg_match($route,$_GET['url'],$matches)>0;

            return $res;
        }else{
            echo "404";
        }
    }

    public function handleSubsequentGetParameters(){
        $matches=[];
        $finalIDList=[];
        //var_dump($_GET['url']);
        preg_match("/(.*?)\/\Kall/",$_GET['url'],$matches);
        //var_dump($matches);
        if(count($matches)>0){
            $finalIDList['id']=$matches[0];
        }else{
            preg_match("/(.*?)\/\K(\d+)$/",$_GET['url'],$matches);
            //var_dump($matches);
            if(count($matches)>0){
                $finalIDList['id']=$matches[0];
            }else{
                preg_match("/(?<=user\/)(.*?)(?=\/tasks)/",$_GET['url'],$matches);
                //var_dump($matches);
                if(count($matches)>0){
                    $finalIDList["user_id"] = $matches[0];
                }
            }
        }
        //var_dump($finalIDList);
        return $finalIDList;
    }
}
