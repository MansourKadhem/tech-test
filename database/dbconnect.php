<?php
/**
 * Created by PhpStorm.
 * User: kadhem
 * Date: 2019-08-22
 * Time: 09:48
 */
namespace database;


class dbconnect
{



    private $host = "";
    private $username = "";
    private $password = "";
    private $dbname = "";

    /**
     * dbconnect constructor.
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $dbname
     */
    public function __construct($host="", $username="", $password="", $dbname="")
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbname;
    }

    /**
     * @return \mysqli
     */
    function connect()
    {
        $mysqli = new \mysqli($this->host, $this->username, $this->password, $this->dbname);
        return $mysqli;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param string $dbname
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }


}
