



function getUsers(callback){
    let url ="/user/all";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "get",
        contentType: "application/json; charset=utf-8",
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function getUser(id,callback){
    let url ="/user/"+id;
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "get",
        contentType: "application/json; charset=utf-8",
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function addUser(data,callback){
    let url ="/user";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            name:data.name,
            email:data.email
        })
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function updateUser(data,callback){
    let url ="/user";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "put",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(
            {
                name:data.name,
                email:data.email,
                id:data.id
            }
        )
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function deleteUser(id,callback){
    let url ="/user";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "delete",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            id:id
        })
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function getTask(id,callback){
    let url ="/task/"+id;
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "get",
        contentType: "application/json; charset=utf-8",
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function getUserTasks(id,callback){
    let url =`/user/${id}/tasks`;
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "get",
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function addTask(data,callback){
    let url ="/task";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            title:data.title,
            description:data.description,
            status:data.status,
            user_id:data.user_id
        })
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}

function updatetask(data,callback){
    let url ="/task";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "put",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            title:data.title,
            description:data.description,
            status:data.status,
            user_id:data.user_id,
            id:data.id
        })
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}
function deleteTask(id,callback){
    let url ="/task";
    // Fire off the request to /form.php
    request = $.ajax({
        url: url,
        type: "delete",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            task_id:id
        })
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        callback?callback(response,null):""
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        callback?callback(null,errorThrown):""
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs

    });
}
